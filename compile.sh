#!/bin/bash
./configure --prefix=/usr --sysconfdir=/etc --libexecdir=/usr/lib --localstatedir=/var --enable-keybinder --enable-libnotify --enable-maintainer-mode
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
make -j3
sudo make install
libtool --finish /usr/local/lib/xfce4/panel/plugins